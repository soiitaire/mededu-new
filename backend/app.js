var express = require('express');
var app = express();

app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "*");
    next();
});

app.use(function (req, res, next) {
    console.log(req.path);
    next();
});

app.get('/', function (req, res) {
    res.send({'word': 'Hello World'});
});


var server = app.listen(3000, function () {
    console.log('listen on port %d', server.address().port);
});
