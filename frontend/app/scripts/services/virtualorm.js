'use strict';

angular.module('frontendApp')
  .factory('$virtualORM', function ($modal, $dialog, _) {
    // Service logic
    // ...


    // Public API here
    return {
      add: function (modalTemplate, modalCtrl, ajaxUrl, lst) {
        var modalInstance = $modal.open({
          templateUrl: modalTemplate,
          controller: modalCtrl,
          resolve: {
            items: function () {
              return {};
            }
          }
        });

        modalInstance.result.then(function (item) {
          //点击确认的回调函数
          //todo api ajaxUrl
          lst.push(item);
        }, function () {
        });
      },

      edit: function (modalTemplate, modalCtrl, ajaxUrl, lst, id) {
        var modalInstance = $modal.open({
          templateUrl: modalTemplate,
          controller: modalCtrl,
          resolve: {
            items: function () {
              return _.findById(lst, id);
            }
          }
        });
        modalInstance.result.then(function (item) {
          //todo api ajaxUrl
          _.editById(lst, id, item);
        }, function () {
        });
      },

      dlt: function (ajaxUrl, lst, id) {
        $dialog.alert('确认要删除该记录么？', function () {
          _.deleteById(lst, id);
        }, function () {
        });
      }
    };
  });
