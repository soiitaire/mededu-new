'use strict';

angular.module('frontendApp')
  .factory('_', function() {

    window._.mixin({
      findById: function(lst, id) {
        return _.find(lst, function (itm) {
          return itm.id == id
        })
      }
    });

    window._.mixin({
      editById: function(lst, id, itm) {
        var idx = window._.indexOf(lst, _.findById(lst, id));
        lst[idx] = itm;
        return lst;
      }
    });

    window._.mixin({
      deleteById: function(lst, id) {
        //var idx = window._.indexOf(lst, _.findById(lst, id));
        //return _.reject(lst, function(itm){ return itm == lst[idx] });
        var idx;
        for (idx in lst) {
          if (lst[idx].id == id) {
            lst.splice(idx);
          }
        }
      }
    });

    return window._;
  });
