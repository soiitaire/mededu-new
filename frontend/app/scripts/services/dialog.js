'use strict';

angular.module('frontendApp')
  .factory('$dialog', function ($modal, _) {
    // Service logic
    // ...

    // Public API here
    return {
      alert: function (message, yesCallback, noCallback) {
        var modalInstance = $modal.open({
          templateUrl: 'views/modal/dialog/confirm.html',
          controller: 'confirmModalCtrl',
          resolve: {
            message: function () {
              return message;
            }
          }
        });
        modalInstance.result.then(yesCallback,noCallback);
      }
    };
  });
