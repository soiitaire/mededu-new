'use strict';

angular.module('frontendApp')
  .controller('confirmModalCtrl', function ($scope, $modalInstance, message) {
    $scope.message = message;
    $scope.yes = function () {
      $modalInstance.close();
    };
    $scope.no = function () {
      $modalInstance.dismiss();
    };
  });
