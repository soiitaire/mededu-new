'use strict';

angular.module('frontendApp')
  .controller('QuestionSetCtrl', function ($scope, $routeParams, $modal, $log, $dialog, $virtualORM, _) {
    $scope.setName = $routeParams.set;

    $scope.setList = [
      {
        'id': 1,
        'name': 'name1',
        'type': 't-or-f',
        'count': 'count1',
        'username': 'username1',
        'udate': 'udate1'
      },
      {
        'id': 2,
        'name': 'name2',
        'type': 'choice',
        'count': 'count2',
        'username': 'username2',
        'udate': 'udate2'
      }
    ];

    $scope.add = function () {
      $virtualORM.add('views/modal/question/set.html', 'QuestionSetModalCtrl', '/', $scope.setList);
    };

    $scope.edit = function (id) {
      $virtualORM.edit('views/modal/question/set.html', 'QuestionSetModalCtrl', '/', $scope.setList, id);
    };

    $scope.remove = function (id) {
      $virtualORM.dlt('/', $scope.setList, id);
    }

  })
  .controller('QuestionSetModalCtrl', function ($scope, $modalInstance, items, _) {
    $scope.items = _.clone(items);
    $scope.types = ['t-or-f', 'choice'];
    $scope.ok = function () {
      $modalInstance.close($scope.items);
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });
