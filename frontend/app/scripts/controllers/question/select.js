'use strict';

angular.module('frontendApp')
  .controller('QuestionSelectCtrl', function ($scope) {
    //todo api
    $scope.paper = {
      'id': 1,
      'name': '标准试卷测试0424'
    };

    //todo api
    $scope.questionList = [
      {
        'id': 'idValue',
        'body': 'bodyValue',
        'typeName': 'typeNameValue',
        'choiceBody': 'choiceBodyValue',
        'answer': 'answerValue'
      }
    ];

    //todo api
    $scope.choiceSetList = [
      {
        'id': 9,
        'name': '基础化学',
        'type': 'general',
        'question_type': 'choice',
        'username': '李佑',
        'count': 1,
        'udate': '2014-04-24'
      },
      {
        'id': 9,
        'name': '基础化学',
        'type': 'general',
        'question_type': 'choice',
        'username': '李佑',
        'count': 1,
        'udate': '2014-04-24'
      }
    ];

    //todo api
    $scope.choiceQuestionList = [
      {
        "id": 78,
        "type": "choice",
        "typeName": "选择题",
        "question_set_id": 9,
        "body": "苯环有几个羟基?",
        "choice": {"D": "3", "A": "6", "B": "4", "C": "8"},
        "choiceBody": "A. 6<br />B. 4<br />C. 8<br />D. 3<br />",
        "choiceJson": "A. 6<br />B. 4<br />C. 8<br />D. 3<br />",
        "choiceType": null,
        "answer": "A",
        "answerSet": ["A"],
        "number": null
      }
    ]
  });
