'use strict';

angular.module('frontendApp')
  .controller('QuestionPaperCtrl', function ($scope, $routeParams, $modal, $log, $dialog, $virtualORM, _) {
    $scope.paperTypeName = $routeParams.set;

    $scope.paperList = [
      {
        'id': 1,
        'name': 'name1',
        'type': $routeParams.set,
        'count': 'count1',
        'username': 'username1',
        'udate': 'udate1'
      },
      {
        'id': 2,
        'name': 'name2',
        'type': $routeParams.set,
        'count': 'count2',
        'username': 'username2',
        'udate': 'udate2'
      }
    ];

    $scope.add = function () {
      $virtualORM.add('views/modal/question/paper.html', 'QuestionSetModalCtrl', '/', $scope.paperList);
    };

    $scope.edit = function (id) {
      $virtualORM.edit('views/modal/question/paper.html', 'QuestionSetModalCtrl', '/', $scope.paperList, id);
    };

    $scope.remove = function (id) {
      $virtualORM.dlt('/', $scope.paperList, id);
    };

  })
  .controller('QuestionPaperModalCtrl', function ($scope, $modalInstance, dict) {
    $scope.items = dict;

    $scope.ok = function () {
      //todo API
      $modalInstance.close($scope.items);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });
