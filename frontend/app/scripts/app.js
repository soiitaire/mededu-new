'use strict';

angular
  .module('frontendApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.bootstrap',
    'pascalprecht.translate'
  ])
  .config(function ($translateProvider) {
    $translateProvider.translations('en', {
      'standard': '标准',
      'general': '普通',
      't-or-f': '判断题',
      'choice': '选择题',
      'long': '问答题',
      'short': '简答题'
    });
    $translateProvider.preferredLanguage('en');
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      //帐号管理
      .when('/account/signin', {
        templateUrl: 'views/account/signin.html',
        controller: 'AccountSigninCtrl'
      })
      .when('/account/recover', {
        templateUrl: 'views/account/recover.html',
        controller: 'AccountRecoverCtrl'
      })
      //题库管理
      .when('/question/:set', {
        templateUrl: 'views/question/set.html',
        controller: 'QuestionSetCtrl'
      })
      .when('/question/:set/paper', {
        templateUrl: 'views/question/paper.html',
        controller: 'QuestionPaperCtrl'
      })
      .when('/question/:set/edit/:id', {
        templateUrl: 'views/question/qa.html',
        controller: 'QuestionQaCtrl'
      })
      .when('/question/:set/paper/edit/:id', {
        templateUrl: 'views/question/select.html',
        controller: 'QuestionSelectCtrl'
      })
      //考试管理
      .when('/exam/:level', {
        templateUrl: 'views/exam/level.html',
        controller: 'ExamLevelCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
