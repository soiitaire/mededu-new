'use strict';

describe('Controller: AccountSigninCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AccountSigninCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AccountSigninCtrl = $controller('AccountSigninCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
