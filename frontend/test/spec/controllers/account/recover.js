'use strict';

describe('Controller: AccountRecoverCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AccountRecoverCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AccountRecoverCtrl = $controller('AccountRecoverCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
