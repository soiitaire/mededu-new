'use strict';

describe('Controller: QuestionPaperCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var QuestionPaperCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QuestionPaperCtrl = $controller('QuestionPaperCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
