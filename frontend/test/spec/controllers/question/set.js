'use strict';

describe('Controller: QuestionSetCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var QuestionSetCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QuestionSetCtrl = $controller('QuestionSetCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
