'use strict';

describe('Controller: QuestionQaCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var QuestionQaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QuestionQaCtrl = $controller('QuestionQaCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
