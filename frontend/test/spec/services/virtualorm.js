'use strict';

describe('Service: virtualORM', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var virtualORM;
  beforeEach(inject(function (_virtualORM_) {
    virtualORM = _virtualORM_;
  }));

  it('should do something', function () {
    expect(!!virtualORM).toBe(true);
  });

});
